import React from 'react'
import { useState } from 'react';
import Swal from "sweetalert2";
import PureModal from 'react-pure-modal';
import 'react-pure-modal/dist/react-pure-modal.min.css';
import { fetchWithToken } from '../../utils/fetchWithToken'




export default function CourseStudents({
    students,
    col1 = 'Nombre',
    col2 = 'Correo electrónico',
    col3 = 'Codigo'
}) {




    const [dialogCalificacion, setDialogCalificacion] = useState(false);
    const [nombreEstudiante, setNombreEstudiante] = useState("nombre");
    const [calificaciones, setCalificaciones] = useState({});
    const [dialogRegistrarCalificacion, setDialogRegistrarCalificacion] = useState(false);



    const [calificacionAdicionalId, setCalificacionAdicionalId] = useState('');
    const [evalComp20, setEvalComp20] = useState('');
    const [coevaluacion20, setCoevaluacion20] = useState('');
    const [cuadernoTrab30, setCuadernoTrab30] = useState('');
    const [trabajos10, setTrabajos10] = useState('');
    const [examen30, setExamen30] = useState('');
    const [promedio, setPromedio] = useState('');
    const [studentId, setStudentId] = useState('');

    const abrirDialogregistrarCalificacion = (studentId) => {
        console.log("STUDENT ID")
        console.log(studentId);
        obtenerCalificacion(studentId);
        setDialogRegistrarCalificacion(true);
        setStudentId(studentId)

    }
    const registrarCalificaciones = () => {

        let obj = {
            calificacionAdicionalId:calificacionAdicionalId,
            studentId: studentId,
            evaluacionCompetencia20: evalComp20,
            coevaluacion20: coevaluacion20,
            cuadernoTrabajo30: cuadernoTrab30,
            trabajos10: trabajos10,
            examen30: examen30,
            promedio: promedio
        };

        fetchWithToken(`api/calificacion-adicional`, obj, "POST")
            .then(res => res.json())
            .then(({ api }) => {
                
                if (api.codeError === "201") {
                    Swal.fire({
                        title: 'Se ha registrado calificaciones exitosamente',
                        confirmButtonText: 'Aceptar',
                        text: api.msgError,
                        confirmButtonColor: "#4fd3d8",
                        cancelButtonColor: "#d33",
                    })
                    setDialogRegistrarCalificacion(false);
                } else {
                    Swal.fire({
                        title: 'No se pudo registrar Evaluaciones',
                        confirmButtonText: 'Aceptar',
                        text: api.msgError,
                        confirmButtonColor: "#4fd3d8",
                        cancelButtonColor: "#d33",
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
    }
    const obtenerCalificacion=(studentId)=>{
        fetchWithToken(`api/calificacion-adicional/${studentId}`, "GET")
        .then(res => res.json())
        .then(({ api, result }) => {

         
            if (api.codeError === "200") {
               setCalificacionAdicionalId(result.calificacionAdicionalId)
               setEvalComp20(result.evaluacionCompetencia20);
               setCoevaluacion20(result.coevaluacion20);
               setCuadernoTrab30(result.cuadernoTrabajo30);
               setTrabajos10(result.trabajos10);
               setExamen30(result.examen30);
               setPromedio(result.promedio)

            } else {
                setCalificacionAdicionalId(0)
                setEvalComp20('');
                setCoevaluacion20('');
                setCuadernoTrab30('');
                setTrabajos10('');
                setExamen30('');
                setPromedio('')
            }
        })
        .catch(err => {
            console.log(err)
        })
    }
    const abrirDialogCalificacion = (name, studentId) => {
        fetchWithToken(`api/calificacion-adicional/${studentId}`, "GET")
            .then(res => res.json())
            .then(({ api, result }) => {

             
                if (api.codeError === "200") {
                    setDialogCalificacion(true);
                    setNombreEstudiante(name);
                    setCalificaciones(result);
                } else {
                    Swal.fire({
                        title: 'No se ha encontrado calificaciones',
                        confirmButtonText: 'Aceptar',
                        text: api.msgError,
                        confirmButtonColor: "#4fd3d8",
                        cancelButtonColor: "#d33",
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })

    }
    const cerrarDialogCalificacion = () => {
        setDialogCalificacion(false);
    }
    const cerrarDialogregistrarCalificacion = () => {
        setDialogRegistrarCalificacion(false);
    }


    return (

        <>
            <style>{`
           table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
          }
          .tituloTabla {
            font-size:10px;
            padding:2px;
          }
          .contenidoTabla {
            font-size:10px;
            padding:2px;
          }
        `}</style>
            <div className="table-responsive">
                <table className="table table-bordered" id="dataTable" width="100%" >
                    <thead>
                        <tr>
                            <th>
                                {
                                    col1
                                }
                            </th>
                            <th>
                                {
                                    col2
                                }
                            </th>
                            <th>
                                {
                                    col3
                                }
                            </th>
                            <th>

                            </th>
                            <th>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            students.map(({
                                name,
                                email,
                                code,
                                studentId
                            }) => (
                                <tr
                                    key={code}
                                >
                                    <td>
                                        {
                                            name
                                        }
                                    </td>
                                    <td>
                                        {
                                            email
                                        }
                                    </td>
                                    <td>
                                        {
                                            code
                                        }
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-outline-secondary btn-sm " onClick={() => abrirDialogCalificacion(name, studentId)} >Ver Evaluación </button>

                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-outline-secondary btn-sm" onClick={() => abrirDialogregistrarCalificacion(studentId)} >Actualizar Evaluación</button>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                <PureModal

                    width="600px"
                    /*footer={
                        <div>
                            <button>Cancel</button>
                            <button>Save</button>
                        </div>
                    }*/
                    isOpen={dialogCalificacion}

                    onClose={() => {
                        cerrarDialogCalificacion();
                        return true;
                    }}
                >
                    <div>
                        <h3>Resumen de Calificación</h3>
                        <table >
                            <thead>
                                <tr>
                                    <th className='tituloTabla' >
                                        Nombre del Alumno
                                    </th>
                                    <th className='tituloTabla'>
                                        Evaluación y Competencia 20%
                                    </th>
                                    <th className='tituloTabla'>
                                        Coevaluación 20%
                                    </th>
                                    <th className='tituloTabla'>
                                        Cuaderno de trabajo 30%
                                    </th>
                                    <th className='tituloTabla'>
                                        Trabajos 10%
                                    </th>
                                    <th className='tituloTabla'>
                                        Examen 30%
                                    </th>
                                    <th className='tituloTabla'>
                                        Promedio
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className='contenidoTabla'>
                                        {nombreEstudiante}
                                    </td>
                                    <td className='contenidoTabla'>
                                        {calificaciones.evaluacionCompetencia20}
                                    </td>
                                    <td className='contenidoTabla'>
                                        {calificaciones.coevaluacion20}
                                    </td>
                                    <td className='contenidoTabla'>
                                        {calificaciones.cuadernoTrabajo30}
                                    </td>
                                    <td className='contenidoTabla'>
                                        {calificaciones.trabajos10}
                                    </td>
                                    <td className='contenidoTabla'>
                                        {calificaciones.examen30}
                                    </td>
                                    <td className='contenidoTabla'>
                                        {calificaciones.promedio}
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </PureModal>;
                <PureModal

                    width="600px"
                    isOpen={dialogRegistrarCalificacion}

                    onClose={() => {
                        cerrarDialogregistrarCalificacion();
                        return true;
                    }}
                >
                    <div>
                        <h3>Actualizar Calificación</h3>
                        <div class="form-group">
                            <label >CALIFICACIÓN - Evaluación y competencia 20%</label>
                            <input type="text" class="form-control" placeholder="Evaluación y competencia 20%" onChange={e => setEvalComp20(e.target.value)} value={evalComp20} />
                        </div>
                        <div class="form-group">
                            <label>CALIFICACIÓN - Coevaluación 20%</label>
                            <input type="text" class="form-control" placeholder="Coevaluación 20%" onChange={e => setCoevaluacion20(e.target.value)} value={coevaluacion20} />
                        </div>
                        <div class="form-group">
                            <label >CALIFICACIÓN - Cuaderno de trabajo 30%</label>
                            <input type="text" class="form-control" placeholder="Cuaderno de trabajo 30%" onChange={e => setCuadernoTrab30(e.target.value)}  value={cuadernoTrab30}/>
                        </div>
                        <div class="form-group">
                            <label >CALIFICACIÓN - Trabajos 10%</label>
                            <input type="text" class="form-control" placeholder="Trabajos 10%" onChange={e => setTrabajos10(e.target.value)} value={trabajos10} />
                        </div>
                        <div class="form-group">
                            <label >CALIFICACIÓN - Examen 30%</label>
                            <input type="text" class="form-control" placeholder="Examen 30%" onChange={e => setExamen30(e.target.value)} value={examen30}/>
                        </div>
                        <div class="form-group">
                            <label >CALIFICACIÓN - Promedio</label>
                            <input type="text" class="form-control" placeholder="Promedio" onChange={e => setPromedio(e.target.value)} value={promedio}/>
                        </div>

                        <button onClick={() => registrarCalificaciones()} class="btn btn-primary">Actualizar Evaluación</button>

                    </div>
                </PureModal>;
            </div>
        </>
    )
}
